package com.dev.ubermensch.compgraphsecondlab;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;


public class ViewFragment extends Fragment {

    private TransformView transformView;

    public ViewFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        transformView = new TransformView(getActivity());
        return transformView;
    }

    public void scaleIn(){
        transformView.scale(0);
    }

    public void scaleOut(){
        transformView.scale(1);
    }

    public void rotateByX(){
        transformView.rotate(0);
    }

    public void rotateByY(){
        transformView.rotate(1);
    }

    public void rotateByZ(){
        transformView.rotate(2);
    }

    public void reflectByX(){
        transformView.reflect(0);
    }

    public void reflectByY(){
        transformView.reflect(1);
    }

    public void reflectByZ(){
        transformView.reflect(2);
    }

    public void resetAll() throws IOException {
        transformView.resetPoints();
        transformView.invalidate();
    }

    public void startAnimation(){
        transformView.toggleAnimation();
    }
}
