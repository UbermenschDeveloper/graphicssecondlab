package com.dev.ubermensch.compgraphsecondlab.transform;

import com.dev.ubermensch.compgraphsecondlab.Transform;

/**
 * Created by asus on 09.11.2017.
 */

public class ScaleTransform extends Transform {

    public static Transform scale(double x, double y, double z){
        Transform transform = getInitTransform(new Transform());
        transform.getTransformMatrix().setRowColValue(0, 0, x);
        transform.getTransformMatrix().setRowColValue(1, 1, y);
        transform.getTransformMatrix().setRowColValue(2, 2, z);
        return transform;
    }

}
