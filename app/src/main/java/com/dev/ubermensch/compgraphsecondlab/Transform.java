package com.dev.ubermensch.compgraphsecondlab;

public class Transform {

    private TransformMatrix transformMatrix;

    public Transform(Transform transform){
        transformMatrix = new TransformMatrix(transform.getTransformMatrix());
    }

    public Transform() {
    }

    public TransformMatrix getTransformMatrix() {
        return transformMatrix;
    }

    public void setTransformMatrix(TransformMatrix transformMatrix) {
        this.transformMatrix = transformMatrix;
    }

    public static Transform getInitTransform(Transform transform){
        transform.setTransformMatrix(TransformMatrix.getInit());
        return transform;
    }

    public double getXTranslation(){
        return transformMatrix.getRowColValue(3, 0);
    }

    public double getYTranslation(){
        return transformMatrix.getRowColValue(3, 1);
    }

    public double getZTranslation(){
        return transformMatrix.getRowColValue(3, 2);
    }

    public void integrateTransforms(Transform transform){
        transformMatrix = transformMatrix.multiplyBy(transform.getTransformMatrix());
    }

    public static Transform perspective(double z){
        Transform transform = getInitTransform(new Transform());
        transform.getTransformMatrix().setRowColValue(2, 2, 0);
        transform.getTransformMatrix().setRowColValue(2, 3, -1/z);
        return transform;
    }

    public Point transformPoint(Point point){
        TransformMatrix transformMatrix = TransformMatrix.getZero();
        transformMatrix.setRowColValue(0, 0, point.getX());
        transformMatrix.setRowColValue(0, 1, point.getY());
        transformMatrix.setRowColValue(0, 2, point.getZ());
        transformMatrix.setRowColValue(0, 3, 1);
        transformMatrix = transformMatrix.multiplyBy(this.transformMatrix);
        double factor = transformMatrix.getRowColValue(0, 3);
        return new Point((int)(transformMatrix.getRowColValue(0, 0)/factor),
                (int)(transformMatrix.getRowColValue(0, 1)/factor),
                (int)(transformMatrix.getRowColValue(0, 2)/factor));
    }
}
