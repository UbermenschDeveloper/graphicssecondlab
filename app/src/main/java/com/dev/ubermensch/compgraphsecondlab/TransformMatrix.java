package com.dev.ubermensch.compgraphsecondlab;


public class TransformMatrix {

    private double [][] matrix = new double[4][4];

    private int len = 4;

    TransformMatrix(TransformMatrix transformMatrix) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                this.matrix[i][j] = transformMatrix.getRowColValue(i, j);
            }
        }
    }

    TransformMatrix(){
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                matrix[i][j] = 0;
            }
        }
    }

    static TransformMatrix getInit(){
        TransformMatrix transformMatrix = new TransformMatrix();
        for(int i = 0; i < 4; i++){
            for (int j = 0; j < 4; j++) {
                double number = 0;
                if(i==j){
                    number = 1;
                }
                transformMatrix.setRowColValue(i, j, number);
            }
        }
        return transformMatrix;
    }

    static TransformMatrix getZero(){
        TransformMatrix result = new TransformMatrix();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result.setRowColValue(i, j, 0);
            }
        }
        return result;
    }

    TransformMatrix multiplyBy(TransformMatrix transformMatrix){
        TransformMatrix result = getZero();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                double number = 0;
                for (int k = 0; k < 4; k++) {
                    number += this.getRowColValue(i, k)* transformMatrix.getRowColValue(k, j);
                }
                result.setRowColValue(i, j, number);
            }
        }
        return result;
    }

    public void setRowColValue(int row, int col, double value){
        matrix[row][col] = value;
    }

    double getRowColValue(int row, int col){
        return matrix[row][col];
    }
}
