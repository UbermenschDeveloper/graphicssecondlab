package com.dev.ubermensch.compgraphsecondlab.transform;

import com.dev.ubermensch.compgraphsecondlab.Transform;



public class TranslationTransform extends Transform {

    public static Transform translate(double x, double y, double z){
        Transform transform = getInitTransform(new Transform());
        transform.getTransformMatrix().setRowColValue(3, 0, x);
        transform.getTransformMatrix().setRowColValue(3, 1, y);
        transform.getTransformMatrix().setRowColValue(3, 2, z);
        return transform;
    }



}
