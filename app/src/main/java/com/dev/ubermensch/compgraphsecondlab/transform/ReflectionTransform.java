package com.dev.ubermensch.compgraphsecondlab.transform;

import com.dev.ubermensch.compgraphsecondlab.Transform;

/**
 * Created by asus on 09.11.2017.
 */

public class ReflectionTransform extends Transform {

    public static Transform reflectByX(){ return ScaleTransform.scale(-1, 1, 1);  }

    public static Transform reflectByY(){
        return ScaleTransform.scale(1, -1, 1);
    }

    public static Transform reflectByZ(){
        return ScaleTransform.scale(1, 1, -1);
    }

}
