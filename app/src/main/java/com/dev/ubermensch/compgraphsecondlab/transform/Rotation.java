package com.dev.ubermensch.compgraphsecondlab.transform;

import com.dev.ubermensch.compgraphsecondlab.Transform;

public class Rotation extends Transform {

    public static Transform rotate(double x, double y, double z){
        Transform transform = getInitTransform(new Transform());
        transform.integrateTransforms(rotateX(x));
        transform.integrateTransforms(rotateY(y));
        transform.integrateTransforms(rotateZ(z));
        return transform;
    }


    public static Transform rotateX(double x){
        Transform transform = getInitTransform(new Transform());
        transform.getTransformMatrix().setRowColValue(1, 1, Math.cos(x));
        transform.getTransformMatrix().setRowColValue(1, 2, -Math.sin(x));
        transform.getTransformMatrix().setRowColValue(2, 1, Math.sin(x));
        transform.getTransformMatrix().setRowColValue(2, 2, Math.cos(x));
        return transform;
    }

    public static Transform rotateY(double y){
        Transform transform = getInitTransform(new Transform());
        transform.getTransformMatrix().setRowColValue(0, 0, Math.cos(y));
        transform.getTransformMatrix().setRowColValue(0, 2, Math.sin(y));
        transform.getTransformMatrix().setRowColValue(2, 0, -Math.sin(y));
        transform.getTransformMatrix().setRowColValue(2, 2, Math.cos(y));
        return transform;
    }

    public static Transform rotateZ(double z){
        Transform transform = getInitTransform(new Transform());
        transform.getTransformMatrix().setRowColValue(0, 0, Math.cos(z));
        transform.getTransformMatrix().setRowColValue(1, 0, -Math.sin(z));
        transform.getTransformMatrix().setRowColValue(0, 1, Math.sin(z));
        transform.getTransformMatrix().setRowColValue(1, 1, Math.cos(z));
        return transform;
    }
}
