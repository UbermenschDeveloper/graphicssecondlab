package com.dev.ubermensch.compgraphsecondlab;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.view.View;

import com.dev.ubermensch.compgraphsecondlab.transform.ReflectionTransform;
import com.dev.ubermensch.compgraphsecondlab.transform.Rotation;
import com.dev.ubermensch.compgraphsecondlab.transform.ScaleTransform;
import com.dev.ubermensch.compgraphsecondlab.transform.TranslationTransform;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;

public class TransformView extends View {

    private ArrayList<Point> basicPoints;
    private Paint paint;
    private double centerX, centerY, centerZ;
    private Transform rotateTransform, scaleTransform, translateTransform, reflectTransform;
    private static Integer maxHeight, maxWidth;
    private double scaleValue;
    private double animAngle = 0;
    private Point animPoint = new Point(0, 0 ,0);
    private CountDownTimer timer;
    private boolean isAnimating;
    private final static int PERSPECTIVE_Z = 700;


    private double xAngle, yAngle, zAngle;

    public TransformView(Context context) {
        super(context);
        try {
            resetPoints();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void getScreenWidth() {
        maxWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static void getScreenHeight() {
        maxHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
    }
    public void toggleAnimation() {
        if (timer == null) {
            int someGreatNumber = 9000000;
            timer = new CountDownTimer(someGreatNumber, 100) {
                public void onTick(long millisUntilFinished) {
                    move();
                    translateTransform = TranslationTransform.translate(translateTransform.getXTranslation() + animPoint.getX(),
                            translateTransform.getYTranslation() + animPoint.getY(),
                            translateTransform.getZTranslation() + animPoint.getZ());
                    invalidate();
                }

                public void onFinish() {
                }

            };
        }
        if(isAnimating){
            killTimer();
        } else {
            timer.start();
            isAnimating = true;
        }
    }

    private void move(){
        animAngle += 0.3;
        int k = 17;
        animPoint.setY((int)(k * Math.pow(Math.cos(animAngle), 2)));
        animPoint.setX((int)(k * Math.pow(Math.cos(animAngle), 2)));
        animPoint.setZ((int)(k * Math.pow(Math.sin(animAngle), 2)));
    }

    public void scale(int param){
        killTimer();
        switch (param){
            case 0:
                scaleValue += 0.1;
                break;
            case 1:
                scaleValue -= 0.1;
                break;
        }
        scaleTransform = ScaleTransform.scale(scaleValue, scaleValue, scaleValue);
        invalidate();
    }

    public void rotate(int param){
        killTimer();
        int dAngle = 11;
        switch (param){
            case 0:
                xAngle += Math.PI/dAngle;
                break;
            case 1:
                yAngle += Math.PI/dAngle;
                break;
            case 2:
                zAngle += Math.PI/dAngle;
                break;
        }
        rotateTransform = Rotation.rotate(xAngle, yAngle, zAngle);
        invalidate();
    }


    public void reflect(int param){
        killTimer();
        switch (param){
            case 0:
                reflectTransform = ReflectionTransform.reflectByX();
                break;
            case 1:
                reflectTransform = ReflectionTransform.reflectByY();
                break;
            case 2:
                reflectTransform = ReflectionTransform.reflectByZ();
                break;
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Transform transformation = calculateTransformations();
        canvas.drawColor(Color.WHITE);
        ArrayList<Point> transformedPoints = new ArrayList<>();
        for(int i = 0; i < basicPoints.size(); i++){
            Point point = transformation.transformPoint(basicPoints.get(i));
            transformedPoints.add(point);
            canvas.drawPoint(point.getX(), point.getY(), paint);
        }
        try {
            assertBounds(transformedPoints);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            link(canvas, transformedPoints);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void link(Canvas canvas, ArrayList<Point> points) throws IOException {

        InputStream is = getResources().openRawResource(R.raw.links);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            is.close();
        }

        String jsonString = writer.toString();

        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            for(int i = 0; i < jsonArray.length(); ++i){
                JSONObject currentPoint = (JSONObject) jsonArray.get(i);
                int a = currentPoint.getInt("a");
                int b = currentPoint.getInt("b");
                canvas.drawLine(points.get(a).getX(), points.get(a).getY(), points.get(b).getX(), points.get(b).getY(), paint);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        invalidate();

    }

    private void setPaint(){
        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(5);
    }

    private void setStartPoints() throws IOException {
        basicPoints = new ArrayList<>();

        InputStream is = getResources().openRawResource(R.raw.points);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            is.close();
        }

        String jsonString = writer.toString();

        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            for(int i = 0; i < jsonArray.length(); ++i){
                JSONObject currentPoint = (JSONObject) jsonArray.get(i);
                int x = currentPoint.getInt("x");
                int y = currentPoint.getInt("y");
                int z = currentPoint.getInt("z");
                basicPoints.add(new Point(x, y, z));
            }

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void center(){
        centerX = 80;
        centerY = 80;
        centerZ = 0;
    }

    private void setStartTransformations(){
        rotateTransform = Transform.getInitTransform(new Rotation());
        translateTransform = Transform.getInitTransform(new TranslationTransform());
        scaleTransform = Transform.getInitTransform(new ScaleTransform());
        reflectTransform = Transform.getInitTransform(new TranslationTransform());
    }

    private Transform calculateTransformations(){
        Transform transform = Transform.getInitTransform(new Transform());
        transform.integrateTransforms(TranslationTransform.translate(-centerX, - centerY, -centerZ));
        transform.integrateTransforms(rotateTransform);
        transform.integrateTransforms(scaleTransform);
        transform.integrateTransforms(translateTransform);
        transform.integrateTransforms(reflectTransform);
        transform.integrateTransforms(TranslationTransform.translate(centerX, centerY, centerZ));
        transform.integrateTransforms(TranslationTransform.translate(maxWidth /2 - centerX,
                    maxHeight /2 - centerY, 0));
        transform.integrateTransforms(Transform.perspective(-PERSPECTIVE_Z));
        return transform;
    }


    public void resetPoints() throws IOException {
        setStartPoints();
        center();
        setStartTransformations();
        setPaint();
        getScreenHeight();
        getScreenWidth();
        scaleValue = 1;
        xAngle = 0;
        yAngle = 0;
        zAngle = 0;
    }

    private void killTimer(){
        if(timer!=null) {
            timer.cancel();
        }
        isAnimating = false;
    }


    private void assertBounds(ArrayList<Point> points) throws IOException {
        for(Point point : points){
            if(point.getX() >= maxWidth ||  point.getY() >= maxHeight ||
                    point.getX() <= 0 || point.getY() <= 0) {
                resetPoints();
                invalidate();
                break;
            }
        }
    }
}