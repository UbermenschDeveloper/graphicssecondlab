package com.dev.ubermensch.compgraphsecondlab;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private ViewFragment viewFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewFragment = new ViewFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.view_cont, viewFragment).commit();
    }

    public void scaleIn(View view){
        viewFragment.scaleIn();
    }

    public void scaleOut(View view){
        viewFragment.scaleOut();
    }

    public void rotateByX(View view){
        viewFragment.rotateByX();
    }

    public void rotateByY(View view){
        viewFragment.rotateByY();
    }

    public void rotateByZ(View view){
        viewFragment.rotateByZ();
    }

    public void reflectByX(View view){
        viewFragment.reflectByX();
    }

    public void reflectByY(View view){
        viewFragment.reflectByY();
    }

    public void reflectByZ(View view){
        viewFragment.reflectByZ();
    }

    public void resetAll(View view) throws IOException {
        viewFragment.resetAll();
    }

    public void animate(View view){
        viewFragment.startAnimation();
    }
}
